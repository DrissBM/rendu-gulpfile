function controls() {
    $(this).addClass('selected').parent().siblings('li').find('button').removeClass('selected');
    var index = $(this).parent().index();
    var mylink = $('.slide');
    $(mylink[index]).siblings().find('a').attr('tabindex', -1);

    $('.wrapper').css("transform", "translateY(" + -$('.slide').height() * index + "px)");

    setTimeout(function(){ 
        $(mylink[index]).find('a').attr('tabindex', 0).focus();
    }, 450);
}
const runCode = () => {
    const mybutton = $('.bullet_point li button');
    if ( mybutton ) {
        $(mybutton).on('click', controls);
    }
}
runCode();
