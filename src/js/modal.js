function toggle() {
    $(this).toggleClass('clicked');
    $('.modal').toggleClass('activated');
    $('body').toggleClass('modal--open');
    if($(this).hasClass('clicked')) {
        trapfocus();
        setTimeout(function(){ 
            $('nav.modal').find('a').first().focus();
        }, 0);
    }
}
function trapfocus() {
    var firstTabbable = $('.burger');
    var lastTabbable = $('nav.modal').find('a').last();

    firstTabbable.on('keydown', function (e) {
        if ((e.which === 9 && e.shiftKey)) {
            e.preventDefault();
            lastTabbable.focus();
        }
    });
    lastTabbable.on('keydown', function (e) {
        if ((e.which === 9 && !e.shiftKey)) {
            e.preventDefault();
            firstTabbable.focus();
        }
    });
}
const go = () => {
    const burger = $('.burger');
    if ( burger ) {
        burger[0].addEventListener('click', toggle, false);
    }
}
go();
