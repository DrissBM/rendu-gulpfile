function toggle() {
    $(this).toggleClass('clicked');
    $('.modal').toggleClass('activated');
    $('body').toggleClass('modal--open');
    if($(this).hasClass('clicked')) {
        trapfocus();
        setTimeout(function(){ 
            $('nav.modal').find('a').first().focus();
        }, 0);
    }
}
function trapfocus() {
    var firstTabbable = $('.burger');
    var lastTabbable = $('nav.modal').find('a').last();

    firstTabbable.on('keydown', function (e) {
        if ((e.which === 9 && e.shiftKey)) {
            e.preventDefault();
            lastTabbable.focus();
        }
    });
    lastTabbable.on('keydown', function (e) {
        if ((e.which === 9 && !e.shiftKey)) {
            e.preventDefault();
            firstTabbable.focus();
        }
    });
}
const go = () => {
    const burger = $('.burger');
    if ( burger ) {
        burger[0].addEventListener('click', toggle, false);
    }
}
go();
;function controls() {
    $(this).addClass('selected').parent().siblings('li').find('button').removeClass('selected');
    var index = $(this).parent().index();
    var mylink = $('.slide');
    $(mylink[index]).siblings().find('a').attr('tabindex', -1);

    $('.wrapper').css("transform", "translateY(" + -$('.slide').height() * index + "px)");

    setTimeout(function(){ 
        $(mylink[index]).find('a').attr('tabindex', 0).focus();
    }, 450);
}
const runCode = () => {
    const mybutton = $('.bullet_point li button');
    if ( mybutton ) {
        $(mybutton).on('click', controls);
    }
}
runCode();
